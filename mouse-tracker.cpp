﻿// mouse-tracker.cpp : Определяет точку входа для приложения.
//

/*
 TODO
  * добавить кнопку, отключающую восприятие событий мыши за счет переменной isCatchingMouseEvents
  * допилить отображение времени в списке событий с миллисекундами
  * добавить воспроизведение движения курсора и отрисовку кликов
*/

#include "framework.h" // подключаются базовые библиотеки windows.h, stdlib.h (стандартная библиотека C), ...
#include "mouse-tracker.h" // заголовочный файл проекта по умолчанию, подключаются стандартные ресурсы
#include "commctrl.h" // контролы, елементы управления. Пока отсюда взят только ListBox

/* следующие 3 библиотеки из состава стандартной библиотеки C++*/
#include <sstream> // stringstream удобный класс для формирования строк из разнородных данных
#include <list> // список чего-либо
#include <chrono> // библиотека для работы со временем

using namespace std; // используем классы и функции стандартной библиотеки без необходимости писать каждый раз std::

#define MAX_LOADSTRING 100 // какая-то стандартно сгенерированная средой константа с ограничениями

// Глобальные переменные:
HINSTANCE hInst;                                // текущий экземпляр главного окна
WCHAR szTitle[MAX_LOADSTRING];                  // Текст строки заголовка
WCHAR szWindowClass[MAX_LOADSTRING];            // имя класса главного окна

HHOOK mouseHook = 0;        // хендлер для системного хука мыши. нужен, чтобы его можно было удалить при выходе
wstring appTitle = L"Mouse events"; // моя строка заголовка приложения. Ее гораздо удобнее использовать
HWND hWindow = 0; // хендлер главного окна. Не совсем правильно, хранить  такие вещи глобально, но мы часто к нему обращаемся, поэтому пойдет.
HWND hListBox = 0; // хендлер списка событий на главном окне
HWND hEnableButton = 0; // хендлер кнопки разрешения отлова событий
int windowWidth = 0; // ширина главного окна
int windowHeight = 0; // высота главного окна

bool isCatchingMouseEvents = true; // отлавливать ли события

list<wstring> mouseEvents{}; // список событий отдельно от интерфейса. пригодится

// Отправить объявления функций, включенных в этот модуль кода:
// Предварительные объявления функций
ATOM                MyRegisterClass(HINSTANCE hInstance); // регистрация класса главного окна
BOOL                InitInstance(HINSTANCE, int); // инициализация зарегистрированного класса
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM); // обработчик основного цикла событий нашего приложения
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM); // окно о программе. сгенерировано
LRESULT CALLBACK    LLMouseProc(int nCode, WPARAM wParam, LPARAM lParam); // обработчик низкоуровневых сообщений от мыши
void SetTitle(const wstring& title); // обновление заголовка окна. можно координаты писать сюда, а можно и не писать
void RefreshWindowSize(); // обновление глобальных переменных ширины и высоты главного окна после изменения его размера
void AddMouseEvent(const wstring& event); // добавление мышиного события в список

// основная точка входа в приложение. отсюда начинается выполнение кода. выход из этой функции означает завершение работы приложения
int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance); // проверка времени компиляции
    UNREFERENCED_PARAMETER(lpCmdLine); // аналогично

    // TODO: Разместите код здесь.

    // Инициализация глобальных строк
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING); // штатный заголовок окна инициализируется
    LoadStringW(hInstance, IDC_MOUSETRACKER, szWindowClass, MAX_LOADSTRING); // штатное название класса окна
    MyRegisterClass(hInstance); // регистрируется класс нашего окна

    // Выполнить инициализацию приложения:
    if (!InitInstance (hInstance, nCmdShow)) // инициализация экземпляра приложения
    {
        return FALSE; // если что-то не так, выходим
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MOUSETRACKER)); // подключение сочетаний клавиш клавиатуры

    MSG msg; // переменная, хранящая последнее принятое сообщение

    // создаем и встраиваем дочернее окно-ListBox, т.е. список на главном окне.
    if ( !(hListBox = CreateWindowEx(NULL, WC_LISTBOX, L"Mouse events", WS_CHILD | WS_VISIBLE | LBS_STANDARD,
        3, 0, windowWidth - 7, windowHeight, // размеры слева отлевой границы окна - 3 пикс., сверху - 0, на всю ширину минус 7 пикс, на всю высоту родительского окна
        hWindow, NULL, hInstance, NULL)) ) {
        return FALSE; // если не получилось, выходим
    }

    // Цикл сообщений:
    while (GetMessage(&msg, nullptr, 0, 0)) // в цикле получаем следующее сообщение из системной очереди сообщений
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg)) // смотрим, не нажато ли сочетание клавиш. обрабатываем, если да
        {
            // если нет 
            TranslateMessage(&msg); // преобразуем возможные нажатия кнопок клавиатуры к нужному формату
            DispatchMessage(&msg); // разбираем, какое сообщение пришло и реагируем соответственно
        }
    }

    return (int) msg.wParam; // завершение 
}


//
//  ФУНКЦИЯ: MyRegisterClass()
//
//  ЦЕЛЬ: Регистрирует класс окна.
//
ATOM MyRegisterClass(HINSTANCE hInstance) 
{
    WNDCLASSEXW wcex; // структура, поля которой надо заполнить и скормить ее функции для регистрации класса

    wcex.cbSize = sizeof(WNDCLASSEX); // заполняется размер структуры

    wcex.style          = CS_HREDRAW | CS_VREDRAW; // сочетание стилей окна
    wcex.lpfnWndProc    = WndProc; // эта функция будет вызываться в ответ на DispatchMessage, обработчик
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance; // хендлер экземпляра нашего приложения, таким образом окно будет создаваться, 
    // работать и уничтожаться в контексте конкретного запущенного экземпляра.
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MOUSETRACKER)); // загружаем ресурс иконки приложения
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW); // загружаем и прописываем курсор
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1); // кисть для заливки окна
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_MOUSETRACKER); // подключаем меню
    wcex.lpszClassName  = szWindowClass; // заданное имя, под которым класс будет известен системе в виде строки
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL)); // мелкая иконка 16х16 пикселей

    return RegisterClassExW(&wcex); // регистрируем класс с параметрами, заданными выше и возвращаемся обратно в main
}

//
//   ФУНКЦИЯ: InitInstance(HINSTANCE, int)
//
//   ЦЕЛЬ: Сохраняет маркер экземпляра и создает главное окно
//
//   КОММЕНТАРИИ:
//
//        В этой функции маркер экземпляра сохраняется в глобальной переменной, а также
//        создается и выводится главное окно программы.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Сохранить маркер экземпляра приложения в глобальной переменной

   RECT desktop; // здесь будем временно хранить координаты рабочего стола для размещения окна в правом углу
   GetClientRect(GetDesktopWindow(), &desktop); // получаем координаты и сохраняем их в переменную выше

   windowWidth = 400; // задаем начальную ширину окна
   windowHeight = 800; // высоту

   // создаем главное окно из ранее зарегистрированного класса в правом нижнем углу экрана
   // с шириной/высотой, заданными ранее + небольшие отступы от краев экрана
   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      desktop.right - windowWidth - 50, desktop.bottom - windowHeight - 50, windowWidth, windowHeight,
        nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE; // если что-то не так, выходим в main с признаком ошибки
   }

   hWindow = hWnd; // сохраняем маркер (хендлер) окна глобально, пригодится

   // вешаем хук на (цепляемся крюком к) системным низкоуровневым (Low Level) событиям мыши
   // в качестве обработчика событий укзываем функцию LLMouseProc, которая написана ниже
   // маркер хука сохраняем глобально
   mouseHook = SetWindowsHookEx(WH_MOUSE_LL, LLMouseProc, hInst, NULL);

   ShowWindow(hWnd, nCmdShow); // показываем окно пользователю
   UpdateWindow(hWnd); // даем команду перерисовать его (сообщение WM_PAINT, которое обрабатывается позднее в WndProc)

   return TRUE; // все ок, возвращаемся в main
}

//
//  ФУНКЦИЯ: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  ЦЕЛЬ: Обрабатывает сообщения в главном окне.
//
//  WM_COMMAND  - обработать меню приложения
//  WM_PAINT    - Отрисовка главного окна
//  WM_DESTROY  - отправить сообщение о выходе и вернуться
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Разобрать выбор в меню:
            switch (wmId)
            {
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Добавьте сюда любой код прорисовки, использующий HDC...
            EndPaint(hWnd, &ps);
            SetWindowText(hWnd, appTitle.c_str()); // обновляем текст заголовка окна из переменной appTitle. Пригодится,
            // если будем писать координаты курсора в заголовок окна
        }
        break;
    case WM_DESTROY:
        ReleaseCapture();
        UnhookWindowsHookEx(mouseHook);
        PostQuitMessage(0);
        break;
    case WM_SIZE: // изменения размеров окна
        // к сожалению, в приложении на чистом WinAPI дочерние окна не изменяют свой размер автоматический, поэтому
        // придется отслеживать изменение размера родительского окна и реагировать
        RefreshWindowSize(); // обновляем глобальные переменные windowWidth, windowHeight для удобства
        // меняем размер ListBox'а согласно новым размерам родительского окна
        SetWindowPos(hListBox, NULL, 3, 0, windowWidth - 7, windowHeight, NULL);
    default:
        return DefWindowProc(hWnd, message, wParam, lParam); // остальные сообщения обрабатываются библиотекой
    }
    return 0;
}

// Обработчик сообщений для окна "О программе".
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

// Обработчик низкоуровневых событий мыши.
// См. "https://docs.microsoft.com/en-us/previous-versions/windows/desktop/legacy/ms644986(v%3Dvs.85)" или https://clck.ru/LVGzn
LRESULT CALLBACK LLMouseProc(int nCode, WPARAM wParam, LPARAM lParam)
{
    if (!isCatchingMouseEvents) // глобальный флаг, надо ли отслеживать сообщения
        return CallNextHookEx(mouseHook, nCode, wParam, lParam); // обработка по умолчанию и выход

    POINT pos{ 0 }; // сюда положим координаты
    GetCursorPos(&pos); // берем координаты курсора и складываем в pos
    const int x = pos.x; // для удобства сохраняем отдельно каждую составляющую координат
    const int y = pos.y; 
    wstringstream event; // здесь будем строить строку из кусочков для списка событий

    using namespace std::chrono; // будем работать с этой библиотекой, но не придется писать этот префикс каждый раз
    
    auto now = system_clock::now(); // текущее время
    // auto t = now.time_since_epoch(); // заготовка

    event << system_clock::to_time_t(now) << ": "; // в строку кладем текущее время
    // добавляем в строку сообщение в зависимости от произошедшего события
    switch (wParam) {
        case WM_LBUTTONDOWN:
            event << "left button pressed at ";
        break;
        case WM_LBUTTONUP:
            event << "left button released at ";
            break;
        case WM_RBUTTONDOWN:
            event << "right button pressed at ";
            break;
        case WM_RBUTTONUP:
            event << "right button released at ";
            break;
        case WM_MOUSEWHEEL:
            event << "wheel scrolled at ";
            break;
        default:
            return CallNextHookEx(mouseHook, nCode, wParam, lParam); // остальные события передаем дальше как есть
    }

    event << L"X: " << x << L"; Y: " << y; // добавляем в строку координаты
    //SetTitle(coords.str()); // при желании можно писать координаты в заголовок окна
    AddMouseEvent(event.str()); // добавляем строку с событием в глобальный список. Может пригодиться
    SendMessage(hListBox, LB_ADDSTRING, 0, (LPARAM) event.str().c_str()); // добавляем строку в ListBox в нашем окне
    RedrawWindow(hWindow, NULL, NULL, RDW_INTERNALPAINT); // говорим окну перерисоваться
    return CallNextHookEx(mouseHook, nCode, wParam, lParam); // вызываем обработку по умолчанию и выходим, т.е. отдаем
    // управление обратно в основной цикл
}

// Просто меняет глобальную переменную, из которой потому будет вытягиваться новый заголовок окна, при необходимости
void SetTitle(const wstring& title)
{
    appTitle = title;
}

// Обновляем добавленные для удобства глобальные переменные, хранящие размеры окна
void RefreshWindowSize()
{
    RECT rect; // сюда положим размеры
    GetClientRect(hWindow, &rect); // берем размеры клиентской области окна (т.е. полные размеры минус границы и меню)
    windowWidth = rect.right - rect.left; // глобальная ширина = правая координата минус левая
    windowHeight = rect.bottom - rect.top; // аналогично для высоты
}

// Добавляем строку с событием в глобальный список
void AddMouseEvent(const wstring& event)
{
    mouseEvents.push_back(event);
}